{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f1d92b61",
   "metadata": {},
   "source": [
    "# Reinforcement Learning : Monte-Carlo Method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eb87e19d",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import random\n",
    "\n",
    "from abc import ABC, abstractmethod\n",
    "from copy import deepcopy\n",
    "from typing import List, Any, Tuple\n",
    "from collections import namedtuple\n",
    "from stochastic.processes.diffusion import VasicekProcess\n",
    "\n",
    "from gym import Env\n",
    "from gym.spaces import Discrete\n",
    "\n",
    "from IPython.display import clear_output\n",
    "\n",
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a38d8f4",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3752fbf",
   "metadata": {},
   "source": [
    "# Constants"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c5dc261",
   "metadata": {},
   "source": [
    "In this task we first model the price move, using Ornstein-Uhlenbeck process \\\n",
    "Then we train the model to trade the security following this price movement \\\n",
    "The trading idea is, in fact, price reversion. The asset has some average price, around which it is oscellating; so the optimal trading strategy here is to buy when the price deviates from the average downwards, and to sell when the price is higher that average. \\\n",
    "However, we will not define the strategy explicitly, but let the Monte-Carlo method learn the data and built it. After that, we age going to see if the result reflects our intuition of the price-reversion"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7545643a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Ornstein-Uhlenbeck constants\n",
    "OU_SPEED       = 1                      # speed of the process\n",
    "OU_MEAN        = 100                    # average price, around which the price is oscellating\n",
    "OU_VOL         = 2                      # volatility\n",
    "OU_NBARS_DAILY = 24*60                  # number of bars during each day | here we assume we trade 24 hours a day, each minute\n",
    "\n",
    "# Discrete process constants\n",
    "OU_DISCRETIZATION_PRICE_STEP    = 0.1   # we transform continious process to discrete values\n",
    "OU_DISCRETIZATION_PRICE_MAX_DEV = 8     # maximum deviation from OU_MEAN, expressed in number of `PRICE-STEP`s\n",
    "\n",
    "TRANSACTION_FEE     = 1e-4              # fee we pay to execute the order, based on the dollar value we trade\n",
    "\n",
    "EPISODE_LEN         = 2048              # length of the episode on which we train the model\n",
    "\n",
    "# Learning constants\n",
    "EXPLORATION_EPSILON = 0.10              # with this probability at each step the model chooses to explore new opportunities, instead of expoiliting the experience it has collected\n",
    "GAMMA               = 0.9               # discout factor of future rewards"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d3a0cd6",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dfeb9a77",
   "metadata": {},
   "source": [
    "# Data Provider"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8eda8cf0",
   "metadata": {},
   "outputs": [],
   "source": [
    "TradingEpisode = namedtuple('TradingEpisode',['price','level'])\n",
    "\n",
    "class BaseDataProvider(ABC):\n",
    "    def __init__(self,**kwargs):\n",
    "        pass\n",
    "    \n",
    "    @abstractmethod\n",
    "    def __len__(self):\n",
    "        pass\n",
    "    \n",
    "    @abstractmethod\n",
    "    def get_episode(self,i,episode_len):\n",
    "        pass\n",
    "\n",
    "    def get_random_episode(self,episode_len):\n",
    "        i = np.random.randint(0,len(self)-episode_len)\n",
    "        return self.get_episode(i,episode_len)\n",
    "\n",
    "class OUDataProvider(BaseDataProvider):\n",
    "    def __init__(self,speed,mean,vol,nbars_daily,ndays,price_step,price_max_dev):\n",
    "        process = VasicekProcess(speed,mean,vol,ndays)\n",
    "        self.data = pd.Series(process.sample(ndays*nbars_daily,initial=mean),index=process.times(ndays*nbars_daily)).to_frame('price')\n",
    "        self.data['level'] = ((self.data['price']-mean)/price_step).round().astype(int)\n",
    "        self.data['level'] = self.data['level'].clip(-price_max_dev,price_max_dev) # we operate with limited window\n",
    "        self.index = self.data.index.values\n",
    "        self.data.index = range(self.data.shape[0])\n",
    "        self.price_max_dev = price_max_dev\n",
    "        \n",
    "    def __len__(self) -> int:\n",
    "        return self.index.size\n",
    "\n",
    "    def get_episode(self,i,episode_len):\n",
    "        return TradingEpisode(\n",
    "            price = self.data['price'].loc[i:i+episode_len],\n",
    "            level = self.data['level'].loc[i:i+episode_len]\n",
    "        )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb00d485",
   "metadata": {},
   "source": [
    "Init train,validation and test data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c17ba1c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# training data\n",
    "DP_train = OUDataProvider(\n",
    "    speed = OU_SPEED,\n",
    "    mean = OU_MEAN,\n",
    "    vol = OU_VOL,\n",
    "    nbars_daily = OU_NBARS_DAILY,\n",
    "    ndays = 900,\n",
    "    price_step = OU_DISCRETIZATION_PRICE_STEP,\n",
    "    price_max_dev = OU_DISCRETIZATION_PRICE_MAX_DEV,\n",
    ")\n",
    "\n",
    "# validation data\n",
    "DP_val = OUDataProvider(\n",
    "    speed = OU_SPEED,\n",
    "    mean = OU_MEAN,\n",
    "    vol = OU_VOL,\n",
    "    nbars_daily = OU_NBARS_DAILY,\n",
    "    ndays = 60,\n",
    "    price_step = OU_DISCRETIZATION_PRICE_STEP,\n",
    "    price_max_dev = OU_DISCRETIZATION_PRICE_MAX_DEV,\n",
    ")\n",
    "\n",
    "# test data\n",
    "DP_test = OUDataProvider(\n",
    "    speed = OU_SPEED,\n",
    "    mean = OU_MEAN,\n",
    "    vol = OU_VOL,\n",
    "    nbars_daily = OU_NBARS_DAILY,\n",
    "    ndays = 30,\n",
    "    price_step = OU_DISCRETIZATION_PRICE_STEP,\n",
    "    price_max_dev = OU_DISCRETIZATION_PRICE_MAX_DEV,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1337268c",
   "metadata": {},
   "source": [
    "To get more understanding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ede2f9e",
   "metadata": {},
   "outputs": [],
   "source": [
    "nrows = 2\n",
    "ncols = 3\n",
    "\n",
    "episodes = [DP_train.get_random_episode(EPISODE_LEN) for i in range(nrows*ncols)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e35bd4ea",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig,ax = plt.subplots(nrows=nrows,ncols=ncols,figsize=(18,6))\n",
    "nrow,ncol = 0,0\n",
    "for i,episode in enumerate(episodes):\n",
    "    ax[nrow,ncol].plot(episode.price.values,color=(np.modf(np.sqrt(0.1)+0.1*i)[0],np.modf(np.sqrt(0.2)+0.2*i)[0],np.modf(np.sqrt(0.3)+0.3*i)[0]))\n",
    "    ncol += 1\n",
    "    if ncol == ncols:\n",
    "        nrow += 1\n",
    "        ncol = 0\n",
    "fig.suptitle('Random episodes',fontsize=12)\n",
    "plt.draw()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "852248b0",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig,ax = plt.subplots(nrows=nrows,ncols=ncols,figsize=(18,6))\n",
    "nrow,ncol = 0,0\n",
    "for i,episode in enumerate(episodes):\n",
    "    ax[nrow,ncol].plot(episode.level.values,color=(np.modf(np.sqrt(0.1)+0.1*i)[0],np.modf(np.sqrt(0.2)+0.2*i)[0],np.modf(np.sqrt(0.3)+0.3*i)[0]))\n",
    "    ncol += 1\n",
    "    if ncol == ncols:\n",
    "        nrow += 1\n",
    "        ncol = 0\n",
    "fig.suptitle('Discrete representation of the above episodes',fontsize=12)\n",
    "plt.draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f494b77c",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c894a1b7",
   "metadata": {},
   "source": [
    "# RL environment"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32f36b53",
   "metadata": {},
   "source": [
    "### Strategy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d2898204",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Strategy:\n",
    "    def __init__(self,transaction_fee):\n",
    "        self.transaction_fee = transaction_fee\n",
    "        self.reset()\n",
    "    \n",
    "    def reset(self,position=None):\n",
    "        self.price = 0.\n",
    "        self.position = position or 0\n",
    "        self.cash = 0.\n",
    "    \n",
    "    def step(self,price,quantity):\n",
    "        fee = price * abs(quantity) * self.transaction_fee\n",
    "        current_reward = self.position * (price-self.price) - fee\n",
    "        \n",
    "        self.price = price\n",
    "        self.position += quantity\n",
    "        self.cash -= price * quantity + fee\n",
    "        \n",
    "        return current_reward\n",
    "    \n",
    "    def value(self): # total value of the strategy\n",
    "        return self.price * self.position + self.cash"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8caa10ef",
   "metadata": {},
   "source": [
    "Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6607921f",
   "metadata": {},
   "outputs": [],
   "source": [
    "s = Strategy(TRANSACTION_FEE)\n",
    "\n",
    "reward,value = [],[]\n",
    "for price,quantity in [(100,1),(101,-1),(100.5,1),(100,-1),(99.5,1),(100,-1),(101,-1),(100,1),(99,1),(100,-1)]: # sequence of trades\n",
    "    reward.append(s.step(price,quantity))\n",
    "    value.append(s.value())\n",
    "\n",
    "pd.DataFrame(dict(\n",
    "    reward = reward,\n",
    "    value = value\n",
    ")).plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b5d4e589",
   "metadata": {},
   "source": [
    "### Market environment"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5456df8f",
   "metadata": {},
   "outputs": [],
   "source": [
    "class MarketEnvironment:\n",
    "    def __init__(self,data_provider,episode_len,transaction_fee):\n",
    "        self.data_provider = data_provider\n",
    "        self.episode_len = episode_len\n",
    "        self.strategy = Strategy(transaction_fee)\n",
    "        \n",
    "        self.n_price_levels = 1+2*self.data_provider.price_max_dev\n",
    "        self.n_position_levels = 3\n",
    "        self.n_states = self.n_price_levels*self.n_position_levels\n",
    "        self.n_actions = 3\n",
    "        \n",
    "        self.start_episode_i = None\n",
    "        \n",
    "    def state_2d_to_1d(self,level,position):\n",
    "        return (level+self.data_provider.price_max_dev)*self.n_position_levels + (position+1)\n",
    "    \n",
    "    def state_1d_to_2d(self,state):\n",
    "        level = state // self.n_position_levels - self.data_provider.price_max_dev\n",
    "        position = state - (level+self.data_provider.price_max_dev)*self.n_position_levels - 1\n",
    "        return level,position\n",
    "    \n",
    "    def init_episode(self,start_episode_i=None):\n",
    "        self.start_episode_i = start_episode_i\n",
    "        if self.start_episode_i is not None:\n",
    "            self.episode = self.data_provider.get_episode(self.start_episode_i,self.episode_len)\n",
    "        else:\n",
    "            self.episode = self.data_provider.get_random_episode(self.episode_len)\n",
    "        self.step_i = 0\n",
    "    \n",
    "    def reset(self,position=None):\n",
    "        if self.start_episode_i is None:\n",
    "            self.episode = self.data_provider.get_random_episode(self.episode_len)\n",
    "        self.strategy.reset(position)\n",
    "        self.step_i = 0\n",
    "\n",
    "        return self.state_2d_to_1d(self.episode.level.iloc[self.step_i],self.strategy.position)\n",
    "    \n",
    "    def current_price(self):\n",
    "        return self.episode.price.iloc[self.step_i]\n",
    "    \n",
    "    def current_level(self):\n",
    "        return self.episode.level.iloc[self.step_i]\n",
    "    \n",
    "    def eligible_actions(self,state):\n",
    "        level,position = self.state_1d_to_2d(state)\n",
    "        \n",
    "        if position == -1:\n",
    "            return [1,2]   # only hold/buy\n",
    "        elif position == 0:\n",
    "            return [0,1,2] # only sell/hold/buy\n",
    "        elif position == 1:\n",
    "            return [0,1]   # only sell/hold\n",
    "        return []\n",
    "    \n",
    "    def step(self,action):\n",
    "        self.step_i += 1\n",
    "        \n",
    "        quantity = 0\n",
    "        if action == 0:    # sell 1 unit\n",
    "            quantity = -1\n",
    "        elif action == 2:  # buy 1 unit\n",
    "            quantity = 1\n",
    "        \n",
    "        reward = self.strategy.step(self.current_price(),quantity)\n",
    "        state = self.state_2d_to_1d(self.current_level(),self.strategy.position)\n",
    "        \n",
    "        return state,reward\n",
    "    \n",
    "    def finished(self):\n",
    "        return self.step_i >= self.episode_len-1\n",
    "    \n",
    "    def gen_Q(self):\n",
    "        Q = np.zeros((self.n_states,self.n_actions))\n",
    "        for state in range(self.n_states):\n",
    "            Q[state,np.setdiff1d(range(self.n_actions),E_train.eligible_actions(state))] = np.nan\n",
    "        return Q"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3952209e",
   "metadata": {},
   "source": [
    "Our observation space consists of\n",
    "- `1+2*OU_DISCRETIZATION_PRICE_MAX_DEV` price levels \\\n",
    "   in our particular case `[-6,-5,-4,...,4,5,6]` (13 points)\n",
    "- 3 position levels, at which we might be any time : holding one short position `(-1)`, holding nothing `(0)`, and holding one long position `(1)` \\\n",
    "\\\n",
    "Totally, we have `3*(1+2*OU_DISCRETIZATION_PRICE_MAX_DEV)` possible states \\\n",
    "\\\n",
    "Each state may be understood as a tuple `(level,position)` \\\n",
    "The range of all possible states may be interpreted as following matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3f31c8ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.DataFrame('-',index=range(-OU_DISCRETIZATION_PRICE_MAX_DEV,1+OU_DISCRETIZATION_PRICE_MAX_DEV),columns=[-1,0,1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7210cbbb",
   "metadata": {},
   "source": [
    "However, dealing with observation matrices is not a convenient way. \\\n",
    "Because, along with observation space we have an action space, which will result in a cube \\\n",
    "For a convenient enumeration of observations, we map matrix into a list of unique integer values, see `MarketEnvironment.state2d_to_state1d()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "895e7a9a",
   "metadata": {},
   "outputs": [],
   "source": [
    "ME = MarketEnvironment(DP_train,EPISODE_LEN,TRANSACTION_FEE)\n",
    "\n",
    "pd.DataFrame([[ME.state_2d_to_1d(level,position) for position in [-1,0,1]] for level in range(-OU_DISCRETIZATION_PRICE_MAX_DEV,1+OU_DISCRETIZATION_PRICE_MAX_DEV)],index=range(-OU_DISCRETIZATION_PRICE_MAX_DEV,1+OU_DISCRETIZATION_PRICE_MAX_DEV),columns=[-1,0,1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6501817a",
   "metadata": {},
   "source": [
    "At each time point of the episode we may perform one of the following actions:\n",
    "- sell 1 unit | ``action=0``\n",
    "  - if we are currently holding short position, we are not allowed to short more\n",
    "  - otherwise sell\n",
    "- hold | ``action=1``\n",
    "- buy 1 unit | ``action=2``\n",
    "  - if we are currently holding long position, we are not allowed to buy more\n",
    "  - otherwise buy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6603b2f6",
   "metadata": {},
   "outputs": [],
   "source": [
    "E_train = MarketEnvironment(DP_train,EPISODE_LEN,TRANSACTION_FEE)\n",
    "\n",
    "E_val = MarketEnvironment(DP_val,len(DP_val),TRANSACTION_FEE)\n",
    "E_val.init_episode(0)\n",
    "\n",
    "E_test = MarketEnvironment(DP_test,len(DP_test),TRANSACTION_FEE)\n",
    "E_test.init_episode(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08a21288",
   "metadata": {},
   "source": [
    "### Policy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15884f00",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Policy:\n",
    "    def __init__(self,E,Q):\n",
    "        # Q is value matrix, must be of shape (E.n_states,E.n_actions)\n",
    "        self.E = E\n",
    "        self.Q = Q\n",
    "    \n",
    "    def random_action(self,state):\n",
    "        return random.choice(self.E.eligible_actions(state))\n",
    "    \n",
    "    def best_action(self,state):\n",
    "        actions = self.E.eligible_actions(state)\n",
    "        q = self.Q[state]\n",
    "        return actions[random.choice(np.where(q[actions]==q[actions].max())[0])]\n",
    "    \n",
    "    def epsilon_greedy_action(self,state,epsilon):\n",
    "        if random.uniform(0,1) < epsilon:        # explore\n",
    "            return self.random_action(state)\n",
    "        else:                                    # exploit\n",
    "            return self.best_action(state)\n",
    "    \n",
    "    def evaluate(self):\n",
    "        state = self.E.reset()\n",
    "        \n",
    "        prices,positions,rewards,values,actions = [],[],[],[],[]\n",
    "        while not self.E.finished():\n",
    "            action = self.epsilon_greedy_action(state,0)\n",
    "            state,reward = self.E.step(action)\n",
    "            \n",
    "            prices.append(self.E.current_price())\n",
    "            positions.append(self.E.strategy.position)\n",
    "            rewards.append(reward)\n",
    "            values.append(self.E.strategy.value())\n",
    "            actions.append(action)\n",
    "        \n",
    "        return prices,positions,rewards,values,actions\n",
    "    \n",
    "    # One run of Monte-Carlo\n",
    "    def run_episode(self,epsilon,quantity=None):\n",
    "        states,actions,rewards = [],[],[]\n",
    "        \n",
    "        state = self.E.reset( np.random.randint(-1,2) if quantity is None else quantity)\n",
    "        while not self.E.finished():\n",
    "            states.append(state)\n",
    "            action = self.epsilon_greedy_action(state,epsilon)\n",
    "            actions.append(action)\n",
    "            state,reward = self.E.step(action)\n",
    "            rewards.append(reward)\n",
    "        \n",
    "        return states,actions,rewards"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9357a2d",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd07a472",
   "metadata": {},
   "source": [
    "# Main part"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2578a1ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "state_size = E_train.n_states\n",
    "action_size = E_train.n_actions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47990907",
   "metadata": {},
   "source": [
    "Value function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43900c42",
   "metadata": {},
   "outputs": [],
   "source": [
    "Q = E_train.gen_Q()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85cfe76e",
   "metadata": {},
   "source": [
    "Policy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a4cbd10e",
   "metadata": {},
   "outputs": [],
   "source": [
    "P_train = Policy(E_train,Q)\n",
    "P_val = Policy(E_val,Q)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81d8dc9b",
   "metadata": {},
   "source": [
    "Main loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "239ed7ec",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "best_final_value = -np.inf\n",
    "best_Q = None\n",
    "best_positions = []\n",
    "best_values = []\n",
    "best_attribution = pd.DataFrame()\n",
    "\n",
    "final_value_hist = []\n",
    "\n",
    "RetSum = np.zeros((state_size,action_size))\n",
    "RetNum = np.zeros((state_size,action_size),dtype=int)\n",
    "\n",
    "# design good criteria of stopping the cycle\n",
    "while True:\n",
    "    # -------------------------------------------------------------------------------------------\n",
    "    # Update block\n",
    "    # -------------------------------------------------------------------------------------------\n",
    "    for i in range(25):\n",
    "        states,actions,rewards = P_train.run_episode(EXPLORATION_EPSILON,0)\n",
    "\n",
    "        # going forward, we accumulate the number of occurences of each (state,action)\n",
    "        counter,count2d = [],np.zeros((state_size,action_size),dtype=int)\n",
    "        for state,action in zip(states,actions):\n",
    "            counter.append(count2d[state,action])\n",
    "            count2d[state,action] += 1\n",
    "\n",
    "        # going backward, we accumulate future value and set it for each (state,action) as long as we reached the first occurence of this (state,action)\n",
    "        G = 0\n",
    "        for t in range(len(rewards)-1,-1,-1):\n",
    "            G *= GAMMA         # discount future reward\n",
    "            G += rewards[t]    # add current reward\n",
    "            if not counter[t]: # first occurence\n",
    "                state,action = states[t],actions[t]\n",
    "                RetSum[state,action] += G\n",
    "                RetNum[state,action] += 1\n",
    "                Q[state,action] = RetSum[state,action] / RetNum[state,action] # update value function\n",
    "\n",
    "    # -------------------------------------------------------------------------------------------\n",
    "    # Evaluation block\n",
    "    # -------------------------------------------------------------------------------------------\n",
    "    prices,positions,rewards,values,actions = P_val.evaluate()\n",
    "    attribution = pd.DataFrame(dict(\n",
    "        SHORT = np.argmax(Q[:E_train.n_price_levels],axis=1), \n",
    "        NO    = np.argmax(Q[E_train.n_price_levels:2*E_train.n_price_levels],axis=1),\n",
    "        LONG  = np.argmax(Q[2*E_train.n_price_levels:],axis=1),\n",
    "    ))\n",
    "    if values[-1] > best_final_value:\n",
    "        best_final_value = values[-1]\n",
    "        best_Q = deepcopy(Q)\n",
    "        best_positions = positions\n",
    "        best_values = values\n",
    "        best_attribution = attribution\n",
    "    final_value_hist.append(values[-1])\n",
    "\n",
    "    # -------------------------------------------------------------------------------------------\n",
    "    # Plot\n",
    "    # -------------------------------------------------------------------------------------------\n",
    "    clear_output(True)\n",
    "    fig,ax = plt.subplots(nrows=3,ncols=2, figsize=(25, 12))\n",
    "    ax[0,0].plot(values)\n",
    "    ax[0,0].set_title('CURRENT')            \n",
    "\n",
    "    ax[0,1].plot(final_value_hist)\n",
    "    ax[0,1].set_title('TOTAL VALUE HISTORY')            \n",
    "\n",
    "    ax[1,0].set_title('BEST')\n",
    "    ax[1,0].plot(best_values)\n",
    "    ax[1,1].set_title('BEST POSITIONS')            \n",
    "    ax[1,1].plot(best_positions)\n",
    "\n",
    "    sns.heatmap(np.transpose(Q),ax=ax[2,0],cmap='viridis')\n",
    "    sns.heatmap(np.transpose(best_Q),ax=ax[2,1],cmap='viridis')\n",
    "    \n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b398e68",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8be0074",
   "metadata": {},
   "source": [
    "Run the optimal strategy on the test process"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "94ea8574",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "ea3262ee",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b49f701",
   "metadata": {},
   "source": [
    "Optimal action matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "80709f41",
   "metadata": {},
   "outputs": [],
   "source": [
    "optimal_action = ((best_Q == np.nanmax(best_Q,axis=1)[:,None]) * np.array([0,1,2])[None,:]).max(axis=1)\n",
    "pd.DataFrame([[optimal_action[ME.state_2d_to_1d(level,position)] for position in [-1,0,1]] for level in range(-OU_DISCRETIZATION_PRICE_MAX_DEV,1+OU_DISCRETIZATION_PRICE_MAX_DEV)],index=range(-OU_DISCRETIZATION_PRICE_MAX_DEV,1+OU_DISCRETIZATION_PRICE_MAX_DEV),columns=[-1,0,1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54014426",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
